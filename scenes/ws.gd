extends Node

export var websocket_url = "ws://192.168.1.60:8888"

var _client = WebSocketClient.new()

signal data_received(data)
signal status_update(status)

var online = false

func _ready():
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	_client.connect("data_received", self, "_on_data")
	
	var err = _client.connect_to_url(websocket_url)
	if err != OK:
		print("Unable to connect", err)
		set_process(false)
		online = false
	
func _closed(was_clean = false):
	print("Closed, clean: ", was_clean)
	self.online = false
	emit_signal("status_update", self.online)
	
func _connected(proto = ""):
	print("Connected")
	self.online = true
	emit_signal("status_update", self.online)

func _on_data():
	var data = JSON.parse(_client.get_peer(1).get_packet().get_string_from_utf8()).result
#	print("Got data from server: ", data)
	emit_signal("data_received", data)

func send(data:Dictionary):
	if online:
		_client.get_peer(1).put_packet(JSON.print(data).to_utf8())
	
func is_online():
	return self.online
	
func _process(delta):
	self._client.poll()
