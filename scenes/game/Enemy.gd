extends KinematicBody

func _ready():
	ws.connect("data_received", self, "data_received")

func data_received(player):
	if player != null:
		print(player)
		transform.basis.x.x = player["basis.x.x"]
		transform.basis.x.y = player["basis.x.y"]
		transform.basis.x.z = player["basis.x.z"]
		transform.basis.y.x = player["basis.y.x"]
		transform.basis.y.y = player["basis.y.y"]
		transform.basis.y.z = player["basis.y.z"]
		transform.basis.z.x = player["basis.z.x"]
		transform.basis.z.y = player["basis.z.y"]
		transform.basis.z.z = player["basis.z.z"]
		transform.origin.x = player["origin.x"]
		transform.origin.y = player["origin.y"]
		transform.origin.z = player["origin.z"]

