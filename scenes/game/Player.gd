extends KinematicBody

export (float, 0, 1.0) var friction = 0.5
export (float, 0, 1.0) var acceleration = 1
export (int) var engine_thrust = 16
export (int) var spin_thrust = 2
export (bool) var is_player = false

onready var ui = $Camera/UI

var velocity = 0
var fwd_thrust = 0
var crosshair_diff = Vector2.ZERO

func _ready():
	if not is_player:
		ui.hide()
		$Camera.current = false
	else:
		$Camera.clear_current()
		if ws.is_online():
			ws.send({"online":true})
	ws.connect("data_received", self, "data_received")
		
	ui.thrust_power_progress.max_value=engine_thrust
	ui.thrust_power_slider.max_value=engine_thrust

func get_input(delta):
	
	if Input.is_action_just_pressed("thrust_add"):
		fwd_thrust += 1
	if Input.is_action_just_pressed("thrust_sub"):
		fwd_thrust -= 1
	
	fwd_thrust = clamp(fwd_thrust, 0, engine_thrust)
	
	ui.thrust_power_progress.value = fwd_thrust
	ui.thrust_power_slider.value = fwd_thrust
	
	crosshair_diff = (ui.center_crosshair - ui.crosshair.rect_position) / ui.rect_size

func _physics_process(delta):
	if get_tree().paused:
		return
	
	if is_player:
		get_input(delta)
		ws.send({"move": true, "fwd": fwd_thrust, "cdx": crosshair_diff.x, "cdy": crosshair_diff.y})
	
	if fwd_thrust > 0:
		velocity = lerp(velocity, fwd_thrust, acceleration * delta)
	else:
		velocity = lerp(velocity, 0, friction * delta)
	
	var a = Quat(global_transform.basis)
	var b1 = Quat(Vector3(0, 0, 1), crosshair_diff.x*spin_thrust*2)
	var b2 = Quat(Vector3(1, 0, 0), crosshair_diff.y*spin_thrust)
	var c = a.slerp(a * b1 * b2, delta)
	
	global_transform.basis = Basis(c)
	
	var fwd = global_transform.basis.xform(Vector3.FORWARD)
	var up = global_transform.basis.xform(Vector3.UP)
	
	move_and_slide((fwd * velocity), up)

func _input(event):
	if get_tree().paused or not is_player:
		return

	if Input.is_action_just_pressed("change_camera"):
		$Camera.current = !$Camera.current

	if event is InputEventMouseButton and event.is_pressed():
		print("Mouse Click/Unclick")

func connected():
	if is_player:
		var pos = global_transform.origin
		ws.send({"pos": true, "px": pos.x, "py": pos.y, "pz": pos.z })
		
func data_received(data):
	if not is_player:
		if "pos" in data:
			global_transform.origin.x = float(data["px"])
			global_transform.origin.x = float(data["py"])
			global_transform.origin.x = float(data["pz"])
		elif "move" in data:
			fwd_thrust = data["fwd"]
			crosshair_diff.x = data["cdx"]
			crosshair_diff.y = data["cdy"]
