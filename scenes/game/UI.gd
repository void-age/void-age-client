extends Control

onready var crosshair = $Crosshair
onready var thrust_power_progress = $ThrustPower/ThrustPowerProgress
onready var thrust_power_slider = $ThrustPower/ThrustPowerSlider

onready var center_crosshair = (rect_size/2 - crosshair.rect_size/2)

var mouse_control = false

func _ready():
	$Blur.visible = false
	crosshair.visible = false

func _input(event):
	if get_tree().paused:
		$Blur.visible = true
		return
	$Blur.visible = false
	
	if event is InputEventKey and event.pressed and event.scancode == KEY_F5:
		mouse_control = !mouse_control
		crosshair.rect_position = center_crosshair
		crosshair.visible = mouse_control
	
	if mouse_control and event is InputEventMouseMotion:
			crosshair.rect_position += event.relative
	
func _process(delta):
	$FPS.text = str(Engine.get_frames_per_second())
	
	if not mouse_control:
		var joy_pos = rect_size/2
		joy_pos.x = Input.get_action_strength("joy_rot_right") - Input.get_action_strength("joy_rot_left")
		joy_pos.y = Input.get_action_strength("joy_rot_down") - Input.get_action_strength("joy_rot_up")
		joy_pos = joy_pos.normalized()*center_crosshair
		crosshair.rect_position = lerp(crosshair.rect_position, joy_pos + center_crosshair, delta*8)
	
	crosshair.rect_position.x = clamp(crosshair.rect_position.x, 0, rect_size.x - crosshair.rect_size.x)
	crosshair.rect_position.y = clamp(crosshair.rect_position.y, 0, rect_size.y - crosshair.rect_size.y)
