extends Spatial

func _unhandled_input(event):
	if (event is InputEventKey and event.pressed and event.scancode == KEY_ESCAPE) or \
	(event is InputEventJoypadButton and event.pressed and event.button_index == 11):
		toggle_pause()
	
func toggle_pause():
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		get_tree().paused = true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		get_tree().paused = false
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
